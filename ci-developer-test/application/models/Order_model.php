<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	
	public function __construct()
	{
		$this->load->database();
	}
	public function get_data($order_id)
	{
		$query = $this->db->get_where('orders', array('orderNumber' => $order_id));

		return $query->row_array();
	}
	public function product_data($order_id)
	{
		$query = $this->db->get_where('orderdetails', array('orderNumber' => $order_id));

		//print_r($query->result_array());
		return $query->result_array();
	}
	public function pdata($p_id)
	{
		$query = $this->db->get_where('products', array('productCode' => $p_id));

		return $query->row_array();
	}
	public function fdata($p_id)
	{
		$query = $this->db->get_where('orderdetails', array('productCode' => $p_id));

		return $query->row_array();
	}
	public function cus($c_id)
	{
		$query = $this->db->get_where('customers', array('customerNumber' => $c_id));
        return $query->row_array();
	}
	public function pay($c_id)
	{
		$query = $this->db->get_where('payments', array('customerNumber' => $c_id));
        return  $query->row(0)->amount;
	}

}

/* End of file Order_model.php */
/* Location: ./application/models/Order_model.php */