
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function login($username,$password)
	{
		$this->db->where('user_name', $username);
		$this->db->where('password', $password);

		$query = $this->db->get('user');

		if($query->num_rows()==1)
		{
		 return  $query->row(0)->user_id;
		
		}
		else
		{
			return false;
		}
	}
	public function check($user_id)
	{
		$query = $this->db->get_where('user', array('user_id' => $user_id));
		$query = $this->db->query("SELECT usertype FROM `user` WHERE `user_id` = 1 ");

		return $query->result();
	}

}

/* End of file User_model.php */
/* Location: ./application/models/User_model.php */