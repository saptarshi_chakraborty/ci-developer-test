<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function index()
	{
		$this->load->view('login');
	}
	public function login()
	{   $user = $this->input->post('Username');
	    $pwd = $this->input->post('Password');
	    
		$user_id = $this->user_model->login($user,$pwd);

		$type = $this->user_model->check($user_id);
		foreach ($type as  $value) {
			if($value->usertype == 'admin')
			{
				$this->load->view('admin');
			}
			else
			{

				$this->load->view('customer');  	}
			}

		}


	}

	/* End of file Users.php */
/* Location: ./application/controllers/Users.php */