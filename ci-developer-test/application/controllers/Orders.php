<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

	public function index()
	{
		$order_id =$this->uri->segment(2);
		
		$myobject = new stdClass();
		$myobject_1 = new stdClass();
		$myobject_2 = new stdClass();
		

		$myobject->order_id = $order_id;

		$order_details_1 = $this->order_model->get_data($order_id);

		$product_details = $this->order_model->product_data($order_id);

		

		foreach ($order_details_1 as $key => $value) {
			if($key == "orderDate" )
			{
				$myobject ->order_date = $value;
			}
			if($key == "status")
			{
				$myobject ->status = $value;
			}
			if($key == "customerNumber")
			{
				$fetchcus = $this->order_model->cus($value);
				$payment = $this->order_model->pay($value);
				$myobject->bill_amount = $payment;
				foreach ($fetchcus as $key => $value) {
					if($key == "contactFirstName" )
					{
						$myobject_2->firstname = $value;
					}

					if($key == "contactLastName" )
					{
						$myobject_2->lastname = $value;
					}
					if($key == "phone" )
					{
						$myobject_2->phone = $value;
					}
					if($key == "country" )
					{
						$myobject_2->country = $value;
					}
				}
			}
		}
		//$product_code = $this->order_model->($order_id);
		$arr = [];

		foreach ($product_details as $key => $value) {
			foreach ($value as $key => $value) {
				//$product_data = $this->order_model->pdata($value);


				if($key == "productCode")
				{
					$product_data = $this->order_model->pdata($value);
					foreach ($product_data as $key => $value) {

						//print_r($value);
						
						if($key == "productCode"){
							$fetch_data = $this->order_model->fdata($value);
							foreach ($fetch_data as $key => $value) {

								if($key == "priceEach")
								{
									$myobject_1->unit_price = $value;
								}
								if($key == "quantityOrdered")
								{
									$myobject_1->qty = $value;
								}
							}
						}if($key == "productName")
						{
							$myobject_1->product = $value;
						}
						if($key == "productLine")
						{
							$myobject_1->productLine = $value;
						}


					}
					$arr[] = $myobject_1;
					$myobject_1 = new stdClass();
				}
			}
		}
		

		$myobject->product_details = $arr;
		$myobject->customer  = $myobject_2;

		$myobject = json_encode($myobject,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

		echo $myobject;
	}

}

/* End of file Orders.php */
/* Location: ./application/controllers/Orders.php */